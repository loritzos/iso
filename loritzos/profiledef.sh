#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="loritzos"
iso_label="LoritzOs_$(date +%Y%m)"
iso_publisher="Gatien Oudoire <https://gitlab.com/loritzos/>"
iso_application="Loritz Os Beta"
iso_version="$(date +%Y.%m.%d)"
install_dir="loritzos"
buildmodes=('iso')
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="erofs"
airootfs_image_tool_options=('-zlz4hc,12')
file_permissions=(
  ["/etc/shadow"]="0:0:400"
  ["/etc/gshadow"]="0:0:400"
)
