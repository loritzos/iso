# génération des locales

locale-gen

# Mettre l'heure sur Paris

ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime


# Clavier en français

echo "KEYMAP=fr" > /etc/vconsole.conf
localectl set-x11-keymap fr


# Installation de gameshell

mkdir /home/loritzos/.scripts/

chmod 0751 /home/loritzos/.scripts/

wget -O /home/loritzos/.scripts/gameshell.sh "https://github.com/phyver/GameShell/releases/download/latest/gameshell.sh"
mkdir /home/loritzos/.scripts
chmod 751 /home/loritzos/.scripts
chmod +x /home/loritzos/.scripts/gameshell.sh


# Installation de lorix

pacman-key --init
wget -O /bin/lorix "https://gitlab.com/loritzos/lorix/-/raw/0.0.1/lorix"
chmod +x /bin/lorix


# Selectionne les mirroirs les plus rapides

reflector --verbose --latest 10 --sort rate --save /etc/pacman.d/mirrorlist

# Met a jour les infos sur le systeme

wget -O /etc/os-release gitlab.com/loritzos/iso/postinstallation/os-release

# Installe les fonds d ecran

mkdir /usr/share/backgrounds

touch /usr/share/backgrounds/loritzos_dark.png

wget -O /usr/share/backgrounds/loritzos_dark.png https://gitlab.com/loritzos/iso/-/raw/plasma/postinstallation/loritzos_dark.png

pip install jupyter
