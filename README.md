# ISO - LoritzOs

La base archiso qui sert à générer l'ISO contenant LoritzOs


## Installation

Pour compiler le projet vous aurez besoin d être sur ArchLinux avec archiso d'installé. Il est possible que la compilation réussisse si vous êtes sur une distribution ayant pour base Arch (ex: Manjaro) depuis qu'un repo local est en place.

Vous devrez ensuite exécuter les commandes suivantes:

```console
$ git clone https://gitlab.com/loritzos/iso.git
```

Il vous faudra entrer vos identifiants GitLab

```console
$ cd iso
$ mkdir {sortie,travail}
$ sudo mkarchiso -v -w travail -o sortie loritzos
```

Le fichier iso sera dans le répertoire sortie

Il se peut que des erreurs surviennent lors de la compilation du au fait que le répertoire local utilise un chemind absolu, par conséquent il vous faudra modifier le fichier "loritzos/pacman.conf".

## Environnement

Le terminal est [Alacritty](https://github.com/alacritty/alacritty).

L'environnement de Bureau est Budgie.

Les éditeurs de textes inclus sont Atom et Nvim.

Le gestionnaire de paquet est [Lorix](https://gitlab.com/loritzos/lorix/) un wrapper de pacman.

Le shell par défaut est zsh.

## Fonctionement

### Fichiers et répertoires

Le répertoire airootfs/ est la racine du systeme live (/)

Le fichier profiledef.sh contient les informations permettant la génération de l'iso, il permet aussi de gérer les permissions

Le fichier packages_x86_64 est la liste des paquets que contient l'iso

Les répertoire syslinux et efiboot contiennent les fichiers permettant de générer le bootloader (comme GRUB)

Le répertoire /var/lib/pacman contient le repo local

Le fichier airootfs/root/customize_airootfs.sh est le script qui est éxécuté à la fin de la génération du système live

### Utilisateurs

L'utilisateur par défaut est loritzos et le mot de passe par défaut est loritz.
(Il est possible que vous ayez à entrer loritw car le clavier est par défaut en anglais)

## Remerciement

Touts les membres du projet mais aussi ceux du Wiki Arch français et anglais qui ont fournis une documentation plus que complète.
